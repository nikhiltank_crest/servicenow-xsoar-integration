import json
from unittest import mock
from unittest.mock import patch
import pytest
import demistomock as demisto
from Servicenow3 import Client
from Servicenow3 import snnikhil_get_knowledge_command, snnikhil_list_knowledge_command, test_module, main


@pytest.fixture
def client():
    return mock.Mock()


def test_snnikhil_get_knowledge_command(client):
    """
    When "snnikhil_get_knowledge_command" command executes successfully then context output and response should match.
    :param client: client object .
    :return: None
    """

    with open('TestData/get-knowledge-response.json', encoding='utf-8') as f:
        expected_res = json.load(f)

    client.http_request.return_value = expected_res

    cmd_res = snnikhil_get_knowledge_command(client, {'sys_id': '00209fc9dbbe1380b5f0715a8c96199f'})

    with open('TestData/get-knowledge-context.json', encoding='utf-8') as f:
        expected_ec = json.load(f)
    assert expected_res == cmd_res.raw_response
    assert expected_ec == cmd_res.outputs

def test_snnikhil_get_knowledge_command_failure(client):
    """
    When response is empty when it should throw exception
    :param client:
    :return: None
    """
    # check for error message
    client.http_request.return_value = {}

    error_msg = snnikhil_get_knowledge_command(client, {'sys_id': '00209fc9dbbe1380b5f0715a8c96199f'})

    assert 'No domain information were found for the given argument(s).' == error_msg


def test_snnikhil_list_knowledge_command_success(client):
    """
    When "snnikhil_list_knowledge_command" command executes successfully then context output and response should match.
    :param client: client object .
    :return: None
    """

    with open('TestData/list-knowledge-response.json', encoding='utf-8') as f:
        expected_res = json.load(f)

    client.http_request.return_value = expected_res

    cmd_res = snnikhil_list_knowledge_command(client, {'sysparm_limit': '1'})

    with open('TestData/list-knowledge-context.json', encoding='utf-8') as f:
        expected_ec = json.load(f)

    assert expected_res == cmd_res.raw_response
    assert expected_ec == cmd_res.outputs


def test_snnikhil_list_knowledge_command_failure(client):
    """
        When response is empty when it should throw exception
        :param client:
        :return: None
    """
    client.http_request.return_value = {}

    error_msg = snnikhil_list_knowledge_command(client, {'sysparm_limit': '1'})

    assert 'No domain information were found for the given argument(s).' == error_msg


# def moke_demisto
@patch('snnikhil-servicenow.Client.http_request')
@patch('snnikhil-servicenow.demisto.params')
@patch('snnikhil-servicenow.demisto.command')
def test_main(client, params, command):
    """
        When "main function called" testmodule should executes successfully check if it work or not.
        :param Client.http_request: http_request is method need to mock .
        :param params: params is method need to mock .
        :param command: command is method need to mock .

        :return: None
    """
    params_dict = {
        'credentials': {
            'identifier': 'guest_user1',
            'password': 'Crest@123'
        },
        'url': 'https://ven02206.service-now.com/'
    }
    client.return_value = 'ok'
    params.return_value = params_dict
    command.return_value = 'test-module'
    main()
