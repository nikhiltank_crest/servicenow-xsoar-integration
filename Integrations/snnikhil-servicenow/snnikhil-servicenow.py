from CommonServerPython import *
from typing import Dict, Any

# Disable insecure warnings
requests.packages.urllib3.disable_warnings()

''' CONSTANTS '''
DATE_FORMAT = '%Y-%m-%dT%H:%M:%SZ'

MESSAGES: Dict[str, str] = {
    'AUTHENTICATION_ERROR': 'Unauthenticated. Check the configured API token and API secret.',
    'PAGE_NOT_FOUND_ERROR': 'No record(s) found.',
    'INTERNAL_SERVER_ERROR': 'The server encountered an internal error for PassiveTotal and was unable to complete '
                             'your request.',
    'NO_RECORDS_FOUND': 'No {0} were found for the given argument(s).'
}


class Client(BaseClient):
    """
    Client will implement the service API, and should not contain any Demisto logic.
    Should only do requests and return data.
    """

    def http_request(self, method, url_suffix):
        """
        :param method:
        :param url_suffix:
        :return:
        """
        resp = self._http_request(method, url_suffix)

        return resp


def test_module(client):
    """
    Returning 'ok' indicates that the integration works like it is supposed to. Connection to the service is successful.

    Args:
        client: ServicenowNikhil client

    Returns:
        'ok' if test passed, anything else will fail the test.
    """

    try:
        client.http_request('GET', url_suffix='/kb_knowledge?sysparm_limit=1')

    except DemistoException:
        return_error('Test connectivity failed. Check the configuration parameters provided.')
    demisto.results('ok')


def snnikhil_get_knowledge_command(client, args):
    """
    snnikhil_get_knowledge command

    Args:
        client (Client): ServicenowNikhil client.
        args (dict): all command arguments.

    Returns:
        Return CommandResults
        outputs_prefix (str): This will be presented prefix of demisto context
        outputs_key_field (str): This will be presented key field name of demisto context
        readable_output (str): This will be presented in the war room - should be in markdown syntax - human readable
        outputs (dict): Dictionary/JSON - saved in the incident context in order to be used as inputs for other tasks in the
                 playbook
        raw_response (dict): Used for debugging/troubleshooting purposes - will be shown only if the command executed with
                      raw-response=true
    """
    sys_id = args.get('sys_id')
    resp = client.http_request('GET',
                               url_suffix='/kb_knowledge/' + sys_id)

    # readable output will be in markdown format - https://www.markdownguide.org/basic-syntax/
    readable_output = f'## \n'
    knowledge_row = resp.get('result')
    if knowledge_row:
        knowledge_result = {
            'sys_id': knowledge_row['sys_id'],
            'number': knowledge_row['number'],
            'short_description': knowledge_row['short_description'],
            'active': knowledge_row['active'],
            'sys_updated_by': knowledge_row['sys_updated_by']
        }
        # set readable output
        readable_output += tableToMarkdown('Knowledge Details:', knowledge_result,
                                           removeNull=True)
        return CommandResults(
            outputs_prefix='SnNikhil.Knowledge',
            outputs_key_field='sys_id',
            outputs=knowledge_result,
            readable_output=readable_output,
            raw_response=resp

        )
    else:
        return MESSAGES['NO_RECORDS_FOUND'].format('domain information')


def snnikhil_list_knowledge_command(client, args):
    """
    snnikhil_list_knowledge command

    Args:
        client (Client): ServicenowNikhil client.
        args (dict): all command arguments.

    Returns:
        Return CommandResults
        outputs_prefix (str): This will be presented prefix of demisto context
        outputs_key_field (str): This will be presented key field name of demisto context
        readable_output (str): This will be presented in the war room - should be in markdown syntax - human readable
        outputs (dict): Dictionary/JSON - saved in the incident context in order to be used as inputs for other tasks in the
                 playbook
        raw_response (dict): Used for debugging/troubleshooting purposes - will be shown only if the command executed with
                      raw-response=true
    """
    sysparm_limit = args.get('sysparm_limit', '')
    resp = client.http_request('GET', url_suffix='/kb_knowledge?sysparm_limit=' + sysparm_limit)

    # readable output will be in markdown format - https://www.markdownguide.org/basic-syntax/
    readable_output = f'#### Records are {sysparm_limit} \n'
    knowledge_result = []
    if resp.get('result', {}):
        for knowledge_row in resp['result']:
            knowledge_result.append({
                'sys_id': knowledge_row['sys_id'],
                'number': knowledge_row['number'],
                'short_description': knowledge_row['short_description'],
                'active': knowledge_row['active'],
                'sys_updated_by': knowledge_row['sys_updated_by']
            })

        readable_output += tableToMarkdown('Knowledge Details:', knowledge_result,
                                           removeNull=True)

        return CommandResults(
            outputs_prefix='SnNikhil.Knowledge',
            outputs_key_field='sys_id',
            outputs=knowledge_result,
            readable_output=readable_output,
            raw_response=resp

        )
    else:
        return MESSAGES['NO_RECORDS_FOUND'].format('domain information')


def main():
    """
        PARSE AND VALIDATE INTEGRATION PARAMS
    """

    username = demisto.params().get('credentials').get('identifier')
    password = demisto.params().get('credentials').get('password')

    # get the service API url
    base_url = urljoin(demisto.params()['url'], '/api/now/table')

    verify_certificate = not demisto.params().get('insecure', False)

    proxy = demisto.params().get('proxy', False)

    LOG(f'Command being called is {demisto.command()}')
    try:
        client = Client(
            base_url=base_url,
            verify=verify_certificate,
            auth=(username, password),
            proxy=proxy)

        if demisto.command() == 'test-module':
            # This is the call made when pressing the integration Test button.
            result = test_module(client)
            demisto.results(result)

        elif demisto.command() == 'snnikhil-list-knowledge':
            return_results(snnikhil_list_knowledge_command(client, demisto.args()))

        elif demisto.command() == 'snnikhil-get-knowledge':
            return_results(snnikhil_get_knowledge_command(client, demisto.args()))
    # Log exceptions
    except Exception as e:
        return_error(f'Failed to execute {demisto.command()} command. Error: {str(e)}')


if __name__ in ('__main__', '__builtin__', 'builtins'):
    main()
